# Base image
FROM node:14

RUN apt-get update && apt-get install -y --fix-missing \
    vim \
    locate \
    git \
    imagemagick \
    graphviz \
    libzip-dev \
    libicu-dev \
    libonig-dev

# Set the working directory
WORKDIR /app

# Bundle app source
COPY . .

# Install dependencies
RUN npm install && npm run build

# Start the application
CMD [ "npm", "run", "start" ]

